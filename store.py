from scraper.scraper.items import Product

class inventory:
    items = {}

    def check_new_item_in_store(self,product: Product):

        if not(product['name'] in self.items) or self.items[product['name']].in_stock != product['in_stock']:
            self.items[product['name']] = product['in_stock']
            return  product['in_stock']
        return False
