FROM python:3.9-slim-buster
RUN apt-get update
RUN apt-get upgrade -y
RUN mkdir /usr/src/app
ADD . /usr/src/app
WORKDIR /usr/src/app
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
RUN chmod +x ./start.sh
ENV TERM=xterm
ENTRYPOINT ./start.sh