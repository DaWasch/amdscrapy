# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class ScraperItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    pass

class LastUpdate(scrapy.Item):
    time = scrapy.Field()

class Product(scrapy.Item):
    name = scrapy.Field()
    price = scrapy.Field()
    link = scrapy.Field()
    in_stock = scrapy.Field()


class ProductList(scrapy.Item):
    lastUpdate = scrapy.Field()
    products = scrapy.Field()