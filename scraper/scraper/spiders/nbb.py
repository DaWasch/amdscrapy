import logging
from datetime import datetime
import pytz
from scrapy.utils.log import configure_logging

import influx_adapter

utc = pytz.utc
utc.zone

import scrapy

from scraper.scraper import items


class NBB(scrapy.Spider):
    product_list = items.ProductList()
    name = "nbb"
    root_url = 'https://www.notebooksbilliger.de';
    custom_settings = {"FEEDS":
                           {"nbb.json":
                                {"format": "json"}
                            }
                       }

    def start_requests(self):
        urls = [
            'https://www.notebooksbilliger.de/pc+hardware/grafikkarten+pc+hardware/amdati/amd+radeon+rx+6000',
            'https://www.notebooksbilliger.de/pc+hardware/grafikkarten/nvidia/geforce+rtx+3000+serie+nvidia'
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        lastUpdate = items.LastUpdate()
        lastUpdate['time'] = datetime.now().astimezone(pytz.timezone('Europe/Berlin')).strftime("%Y-%m-%dT%H:%M:%S")

        for item in response.css('div.js-listing-item-GTM'):
            product = None
            in_stock = None
            price = None

            product = item.css('a.listing_product_title::attr(title)').get()
            price = item.attrib['data-price']
            link = item.css('a.listing_product_title::attr(href)').get()
            in_stock = item.css('div.availability').css('span.highlight_green').css('span.highlight_green::text').re('.*(sofort ab Lager).*')

            p = items.Product()
            pl = []
            link = response.urljoin(link)
            p['name'] = product
            p['price'] = price
            p['in_stock'] = True if in_stock and in_stock[0] == 'sofort ab Lager' else False
            p['link'] = link

            influx_adapter.write_influx_point(p, self.name)
            pl.append(p)


            product_list = items.ProductList()
            product_list['products'] = pl
            product_list['lastUpdate'] = lastUpdate

            yield product_list
