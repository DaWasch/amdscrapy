import logging
from datetime import datetime
import pytz
import influx_adapter

utc = pytz.utc
utc.zone

import scrapy

from scraper.scraper import items


class MINDFACTORY(scrapy.Spider):
    product_list = items.ProductList()
    name = "mindfactory"
    root_url = 'https://www.mindfactory.de/';
    custom_settings = {"FEEDS":
                           {"mindfactory.json":
                                {"format": "json"}
                            }
                       }

    def start_requests(self):
        urls = [
            'https://www.mindfactory.de/Hardware/Grafikkarten+(VGA)/GeForce+RTX+fuer+Gaming.html',
            'https://www.mindfactory.de/Hardware/Grafikkarten+(VGA)/Radeon+RX+Serie.html'
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        lastUpdate = items.LastUpdate()
        lastUpdate['time'] = datetime.now().astimezone(pytz.timezone('Europe/Berlin')).strftime("%Y-%m-%dT%H:%M:%S")

        for item in response.css('div.pcontent'):
            product = None
            in_stock = None
            price = None

            product = item.css('div.pimg').css('img::attr(alt)').get()
            price = item.css('div.pprice').re('\s(\d*\.*\d+,*\d*)')
            link = item.css('a.p-complete-link::attr(href)').get()
            in_stock = item.css('span.shipping1::text').re('.*(Lagernd).*')
            in_stock_2 = item.css('span.shipping2::text').re('.*(Verfügbar).*')

            p = items.Product()
            pl = []
            link = response.urljoin(link)
            p['name'] = product
            p['price'] = price[0].replace('.','') if price and price[0] else None
            p['in_stock'] = True if in_stock and in_stock[0] == 'Lagernd' else True if in_stock_2 and in_stock_2[0] == 'Verfügbar' else False
            p['link'] = link

            influx_adapter.write_influx_point(p, self.name)
            pl.append(p)


            product_list = items.ProductList()
            product_list['products'] = pl
            product_list['lastUpdate'] = lastUpdate

            yield product_list
