import logging
from datetime import datetime

import pytz

import influx_adapter

utc = pytz.utc
utc.zone

import scrapy

from scraper.scraper import items


class AMD(scrapy.Spider):
    product_list = items.ProductList()
    name = "amd"
    root_url = 'https://www.amd.com';
    custom_settings = {"FEEDS":
                           {"amd.json":
                                {"format": "json"}
                            },
                       "DOWNLOADER_MIDDLEWARES" : {
                                                    'scrapy.downloadermiddlewares.useragent.UserAgentMiddleware': 500,
                                                    'scrapy_useragents.downloadermiddlewares.useragents.UserAgentsMiddleware': None,
                                                 },
                       "USER_AGENT":'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:87.0) Gecko/20100101 Firefox/87.0'
                       }

    def start_requests(self):
        urls = [
            'https://www.amd.com/de/direct-buy/de',
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        lastUpdate = items.LastUpdate()
        lastUpdate['time'] = datetime.now().astimezone(pytz.timezone('Europe/Berlin')).strftime("%Y-%m-%dT%H:%M:%S")

        for item in response.css('div.shop-content'):
            product = None
            in_stock = None
            price = None

            product = item.css('div.shop-title::text').re('(\S(\s|.)*\S+)')
            price = item.css('div.shop-price::text').re('\d+[\.|,]*\d*')
            link = item.css('a.shop-image-link::attr(href)').get()
            in_stock = item.css('div.shop-links').css('button.btn-shopping-cart::text').re('.*(Add to cart).*')

            p = items.Product()
            pl = []
            if len(in_stock) == 0 and (len(link) > 0):
                yield response.follow(self.root_url + link, callback=self.parse_additional_product, cb_kwargs=dict(p = p))

                logging.info(p)
            else:
                link = response.urljoin(link)
                p['name'] = product[0] if product and len(product)>0 else None
                p['price'] = price[0].replace(",",".") if price and len(price)>0 else None
                p['in_stock'] = True if in_stock and in_stock[0] == 'Add to cart' else False
                p['link'] = link

                influx_adapter.write_influx_point(p, self.name)
                pl.append(p)


                product_list = items.ProductList()
                product_list['products'] = pl
                product_list['lastUpdate'] = lastUpdate

                yield product_list

    def parse_additional_product(self,response,p):
        lastUpdate = items.LastUpdate()
        lastUpdate['time'] = datetime.now().astimezone(pytz.timezone('Europe/Berlin')).strftime("%Y-%m-%dT%H:%M:%S")

        product = response.css('h1.page-title::text').re('(\S(\s|.)*\S+)')
        in_stock = response.css('button.btn-shopping-cart::text').re('.*(Add to cart).*')
        price = response.css('div.product-page-description').css('h4::text').re('\d+[\.|,]*\d*')
        link = response.url

        p['name'] = product[0] if product and len(product) > 0 else None
        p['price'] = price[0].replace(',','.') if price and len(price) > 0 else None
        p['in_stock'] = True if in_stock and in_stock[0] == 'Add to cart' else False
        p['link'] = link
        influx_adapter.write_influx_point(p, self.name)
        pl = []

        pl.append(p)

        product_list = items.ProductList()
        product_list['products'] = pl
        product_list['lastUpdate'] = lastUpdate

        yield product_list
        #
        # product_list = items.ProductList()
        # product_list['products'] = self.pl
        # product_list['lastUpdate'] = lastUpdate
        # yield  product_list