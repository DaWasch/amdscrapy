import os
from multiprocessing import Process, Queue

from scrapy import crawler
from scrapy.utils.project import get_project_settings
from twisted.internet import reactor


def f(q, spider):
    settings_file_path = 'scraper.scraper.settings' # The path seen from root, ie. from main.py
    os.environ.setdefault('SCRAPY_SETTINGS_MODULE', settings_file_path)
    try:
        runner = crawler.CrawlerRunner(get_project_settings())
        deferred = runner.crawl(spider)
        deferred.addBoth(lambda _: reactor.stop())
        reactor.run()
        q.put(None)
    except Exception as e:
        q.put(e)


def run_spider(spider):
    q = Queue()
    p = Process(target=f, args=(q, spider,))
    p.start()
    result = q.get()
    p.join()

    if result is not None:
        raise result