import logging
import re

import influxdb_client
from influxdb_client.client.write_api import SYNCHRONOUS

from env import influx_env


def write_influx_point(product,spider_name):
   price = None

   if influx_env.get_influx_flag():
      if product['price'] \
              and len(product['price'])>0 \
              and re.search('\d+[,\.]*\d+',product['price']):
         price = float(re.findall('\d+[,\.]*\d+',product['price'])[0].replace(',', '.'))


      client = influxdb_client.InfluxDBClient(
         url=influx_env.get_influx_url(),
         token=influx_env.get_influx_token(),
         org=influx_env.get_influx_org()
      )
      write_api = client.write_api(write_options=SYNCHRONOUS)

      p = influxdb_client.Point("availability")\
         .tag("product", product['name'])\
         .tag("spider",spider_name)\
         .tag("link",product['link'])\
         .field("in_stock",product['in_stock'])\
         .field("price", price)

      logging.info('influx write' + str(product))
      write_api.write(bucket=influx_env.get_influx_bucket(), org=influx_env.get_influx_org(), record=p)
