import os


class signal_env:
    @staticmethod
    def get_signal_flag():
        return os.getenv("SIGNAL_FLAG", "false") == "true"

    @staticmethod
    def get_signal_cli_rest_url():
        return os.getenv("SIGNAL_CLI_REST_URL", None)

    @staticmethod
    def get_signal_sender():
        return os.getenv("SIGNAL_SENDER", None)

    @staticmethod
    def get_signal_receipients():
        return os.getenv("SIGNAL_RECEIPIENTS",None)


class influx_env:
    @staticmethod
    def get_influx_flag():
        return os.getenv("INFLUXDB2_FLAG", "false") == "true"

    @staticmethod
    def get_influx_bucket():
        return os.getenv("INFLUXDB2_BUCKET", None)

    @staticmethod
    def get_influx_org():
        return os.getenv("INFLUXDB2_ORG", None)

    @staticmethod
    def get_influx_token():
        return os.getenv("INFLUXDB2_TOKEN",None)

    @staticmethod
    def get_influx_url():
        return os.getenv("INFLUXDB2_URL",None)
    
class nordvpn:
    @staticmethod
    def get_nordvpn_flag():        
        return os.getenv("NORDVPN_ROTATE", "false") == "true"