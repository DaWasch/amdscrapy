import logging
import re
import requests
from env import signal_env


def send_signal_message(message):
    logging.info("Start" + str(signal_env.get_signal_flag()))
    if signal_env.get_signal_flag():
        message_json = {
            "message": str(message).replace('[\'','').replace('\']','\n\n'),
            "number": signal_env.get_signal_sender(),
            "recipients": re.findall(r';?([\w\.=\d\+]+);?',signal_env.get_signal_receipients())
        }
        try:
            x = requests.post(signal_env.get_signal_cli_rest_url() + '/v2/send', json=message_json)
            logging.info("Send Signal Message: " + str(x))
        except:
            logging.info("request to signal server failed")
