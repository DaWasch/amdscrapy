import json
import logging
import os
import random
import re
import threading
import time
import webbrowser
from datetime import datetime, timedelta
import nordvpn_switcher
import env
import multiprocessing

import pytz

import signal_adapter
import store
from scraper.scraper.spiders.amd import AMD
from scraper.scraper.spiders.mindfactory import MINDFACTORY
from scraper.scraper.spiders.nbb import NBB
from dotenv import load_dotenv

load_dotenv()  # take environment variables from .env.

utc = pytz.utc
utc.zone

from scrapy.utils.log import configure_logging

from scraper.scraper.run_scraper import run_spider

already_opend = {}
inventory = store.inventory()
jobs = []


spiders = [
    {"spider": AMD,
     "file_name": "amd.json",
     "next_update": None,
     "retention":5
     },
    {"spider": NBB,
     "file_name": "nbb.json",
     "next_update": None,
     "retention":20,
     "random":3,
     },
    {"spider": MINDFACTORY,
     "file_name": "mindfactory.json",
     "next_update": None,
     "retention":20,
     "random":3,
     }
]

gpus = [
    {
        "chip": "RX6700XT",
        "max_price": 750
    },
    {
        "chip": "RX6800XT",
        "max_price": 1000
    },
    {
        "chip": "RX6800",
        "max_price": 900
    },
    {
        "chip": "RX6900XT",
        "max_price": 1500
    },
    {
        "chip": "RTX3090",
        "max_price": 2000
    },
    {
        "chip": "RTX3080",
        "max_price": 1100
    },
    {
        "chip": "RTX3080TI",
        "max_price": 1500
    },
    {
        "chip": "RTX3070",
        "max_price": 1000
    },
    {
        "chip": "RTX3060TI",
        "max_price": 900
    },
    {
        "chip": "RTX3060",
        "max_price": 600
    },
]

def check_price(product_name, price):
    for gpu in gpus:
        p = re.compile("("+ gpu['chip'] + ")")
        if p.search(product_name.upper().replace(" ","")):
            if float(price.replace(",",".")) <= gpu['max_price']:
                return True
            else:
                return False
    return None


def notify_if_price_is_right(p):
    message = None
    in_stock = 'Out of stock'
    if p['name']:
        check = False
        if p['in_stock']:
            check = check_price(p['name'],p['price'])

        if p['in_stock'] and check:
            in_stock = 'In stock'
            if not (p['link'] in already_opend):
                webbrowser.open(p['link'])
                message = p['name'] + " verfügbar! " + p['link'] + " " + p['price']
                already_opend[p['link']]={"last_update" : datetime.now()}
        else:
            if check != None and not(check) and p['in_stock']:
                in_stock = "too expensive"
            if check == None:
                in_stock = "available but not interested in"

            if p['link'] in already_opend and \
                    already_opend[p['link']]["last_update"] < datetime.now() - timedelta(minutes=15):
                del already_opend[p['link']]

        if in_stock and p['name'] and p['price'] and p['link']:
            logging.info(
                in_stock.rjust(33) + ': ' + p['name'][:54].ljust(54) + ' ' + p['price'].rjust(9) + ' ' + p['link'])
    return message

def parse_file(file_name):
    if os.stat(file_name).st_size > 0:
        with open(file_name) as json_file:
            try:
                data = json.load(json_file)
                signal_message = []
                for dt in data:
                    p = dt['products'][0]
                    message = notify_if_price_is_right(p)
                    if message != None:
                        signal_message.append(message)
                if len(signal_message)>0:
                    async_proc = threading.Thread(target=signal_adapter.send_signal_message,args=(signal_message,))
                    async_proc.start()
                    # signal_adapter.send_signal_message(signal_message)
            except:
                logging.exception('Crawling failed')
    else:
        logging.warning('no crawling results')

def crawl(spider):
    if spider['next_update'] == None or datetime.now()> spider['next_update']:
        logging.info('crawl ' + spider['spider'].name)
        if os.path.exists(spider['file_name']):
            os.remove(spider['file_name'])
        run_spider(spider['spider'])
        retention = spider['retention']
        if 'random' in spider:
            retention = random.randint(-1*spider['random'], spider['random']) + retention

        spider['next_update'] = datetime.now()+timedelta(seconds=retention)


    parse_file(spider['file_name'])

def call_crawler():
    # Clean display
    #os.system('cls' if os.name == 'nt' else 'clear')
    configure_logging({'LOG_FORMAT': '%(asctime)s: %(levelname)s: %(message)s', 'LOG_LEVEL': 'INFO'})
    for spider in spiders:
        crawl(spider)


def crawl_loop():
    while True:
        call_crawler()
        time.sleep(5)


if __name__ == '__main__':
    crawl_loop()
